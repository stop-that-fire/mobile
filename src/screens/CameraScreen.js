import React, { Component } from 'react';
import { Screen, Icon } from '@shoutem/ui';
import { RNCamera } from 'react-native-camera';
import { StyleSheet, TouchableOpacity } from 'react-native';

class CameraScreen extends Component {
  takePicture = async () => {
    if (this.camera) {
      const options = { quality: 0.5, base64: true };
      const data = await this.camera.takePictureAsync(options);
      this.props.navigation.goBack(null);
      this.props.navigation.state.params.onCapture({ uri: data.uri });
    }
  };

  render() {
    return (
      <Screen>
        <RNCamera
          ref={ref => {
            this.camera = ref;
          }}
          style={styles.preview}
          type={RNCamera.Constants.Type.back}
          flashMode={RNCamera.Constants.FlashMode.on}
          captureAudio={false}
          androidCameraPermissionOptions={{
            title: 'Permission to use camera',
            message: 'We need your permission to use your camera',
            buttonPositive: 'Ok',
            buttonNegative: 'Cancel'
          }}
        />
        <TouchableOpacity
          style={{
            position: 'absolute',
            top: 50,
            left: 20,
            fontSize: 30,
            backgroundColor: 'rgba(52, 52, 52, 0.3)',
            color: 'white',
            padding: 5,
            borderRadius: 20
          }}
          onPress={() => this.props.navigation.goBack(null)}
        >
          <Icon
            name="close"
            style={{
              color: 'white'
            }}
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            alignSelf: 'center',
            position: 'absolute',
            bottom: 50,
            height: 60,
            width: 60,
            backgroundColor: '#e0e0e0',
            borderRadius: 30,
            borderWidth: 10,
            borderColor: '#ffffff'
          }}
          onPress={() => this.takePicture()}
        ></TouchableOpacity>
      </Screen>
    );
  }
}

const styles = StyleSheet.create({
  preview: {
    flex: 1,
    position: 'relative'
  },
  capture: {}
});

export default CameraScreen;
