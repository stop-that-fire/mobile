import React, { Component } from 'react';
import {
  View,
  Icon,
  Image,
  Screen,
  Button,
  ListView,
  TextInput
} from '@shoutem/ui';
import { KeyboardAvoidingView } from 'react-native';

class CreatePostScreen extends Component {
  state = {
    pictures: []
  };

  onCapture = data => {
    const pictures = [data, ...this.state.pictures];
    this.setState({ pictures });
  };

  renderRow = image => {
    if (!image) {
      return null;
    }

    return (
      <View
        style={{ paddingLeft: 5, paddingRight: 5, backgroundColor: 'white' }}
      >
        <Image
          style={{ borderRadius: 10, height: 70, width: 70 }}
          source={{ uri: image.uri }}
        />
      </View>
    );
  };

  render() {
    return (
      <Screen>
        <KeyboardAvoidingView>
          <TextInput
            placeholder={"What's Happening?"}
            autoFocus={true}
            multiline={true}
            onChangeText={() => {}}
            style={{
              height: 300
            }}
          />
          <View
            styleName="horizontal"
            style={{ padding: 10, backgroundColor: 'white' }}
          >
            <Button
              style={{
                borderWidth: 1,
                borderColor: 'tomato',
                borderRadius: 10,
                height: 70,
                width: 70,
                marginRight: 10
              }}
              onPress={() =>
                this.props.navigation.navigate('Camera', {
                  onCapture: this.onCapture
                })
              }
            >
              <Icon
                style={{ color: 'tomato', fontSize: 30 }}
                name="take-a-photo"
              />
            </Button>

            <ListView
              data={this.state.pictures}
              renderRow={this.renderRow}
              horizontal={true}
            />
          </View>
        </KeyboardAvoidingView>
      </Screen>
    );
  }
}

export default CreatePostScreen;
