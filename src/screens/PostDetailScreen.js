import React, { Component } from 'react';
import { ScrollView } from 'react-native';
import {
  Row,
  Text,
  View,
  Image,
  Screen,
  Caption,
  Divider,
  Subtitle,
  InlineGallery
} from '@shoutem/ui';

import MapViewComponent from './../components/MapViewComponent';
import AvatarBubble from './../components/AvatarBubble';

const LATITUDE_DELTA = 0.01;
const LONGITUDE_DELTA = 0.01;

class PostDetailScreen extends Component {
  render() {
    const { post } = this.props.navigation.state.params;
    return (
      <Screen>
        <ScrollView>
          <InlineGallery
            styleName="large-wide"
            data={[{ source: { uri: post.image.url } }]}
          />
          <Row>
            <Image
              styleName="small-avatar top"
              source={{
                uri: post.user.pic
              }}
            />
            <View styleName="vertical">
              <View styleName="horizontal space-between">
                <Subtitle>{post.user.name}</Subtitle>
                <Caption>{post.time}</Caption>
              </View>
              <View>
                <Caption>{post.address}</Caption>
              </View>
              <Text styleName="multiline">{post.description}</Text>
            </View>
          </Row>
          <View pointerEvents="none">
            <MapViewComponent
              zoomEnabled={false}
              style={{ height: 300 }}
              initialRegion={{
                latitude: post.coordinate.latitude,
                longitude: post.coordinate.longitude,
                latitudeDelta: LATITUDE_DELTA,
                longitudeDelta: LONGITUDE_DELTA
              }}
              markers={[
                {
                  latitude: post.coordinate.latitude,
                  longitude: post.coordinate.longitude
                }
              ]}
            />
          </View>
          <Divider styleName="section-header">
            <Caption>{post.verifiedBy.length} People Verified</Caption>
          </Divider>
          <View style={{ padding: 10 }}>
            <AvatarBubble avatars={post.verifiedBy} />
          </View>
        </ScrollView>
      </Screen>
    );
  }
}

export default PostDetailScreen;
