import React, { Component } from 'react';
import {
  View,
  Icon,
  Image,
  Screen,
  Divider,
  Caption,
  ListView,
  Subtitle,
  TouchableOpacity
} from '@shoutem/ui';
import ActionButton from 'react-native-action-button';

const croudsourceData = [
  {
    user: {
      name: 'Tom Travolta',
      pic: 'https://i.imgur.com/zpgE5G3.jpg'
    },
    address: 'Leknath Marg, Kathmandu 44600',
    time: '20 minutes ago',
    coordinate: {
      latitude: 27.716904,
      longitude: 85.315849
    },
    image: {
      url: 'https://i.imgur.com/5PJn4BS.png'
    },
    description:
      'Banjo tote bag bicycle rights, High Life sartorial cray craft beer whatever street art fap. Hashtag typewriter banh mi, squid keffiyeh High.',
    verifiedBy: [
      'https://i.imgur.com/raKYbaO.jpg',
      'https://i.imgur.com/m26Pzgj.jpg',
      'https://i.imgur.com/o5dNDkT.jpg',
      'https://i.imgur.com/QCeRNNF.jpg'
    ]
  },
  {
    user: {
      name: 'Claire Travolta',
      pic: 'https://i.imgur.com/m26Pzgj.jpg'
    },
    address: 'Madhyapur Thimi 44600',
    time: '3 hours ago',
    coordinate: {
      latitude: 27.671703,
      longitude: 85.360454
    },
    image: {
      url: 'https://i.imgur.com/TKcNsq7.jpg'
    },
    description:
      'Banjo tote bag bicycle rights, High Life sartorial cray craft beer whatever street art fap. Hashtag typewriter banh mi, squid keffiyeh High.',
    verifiedBy: [
      'https://i.imgur.com/zpgE5G3.jpg',
      'https://i.imgur.com/o5dNDkT.jpg'
    ]
  },
  {
    user: {
      name: 'Jeff Cranston',
      pic: 'https://i.imgur.com/o5dNDkT.jpg'
    },
    address: 'Chhatre Deurali 45100',
    time: '4 hours ago',
    coordinate: {
      latitude: 27.753487,
      longitude: 85.226118
    },
    image: {
      url: 'https://i.imgur.com/6Ft9RIs.jpg'
    },
    description:
      'Banjo tote bag bicycle rights, High Life sartorial cray craft beer whatever street art fap. Hashtag typewriter banh mi, squid keffiyeh High.',
    verifiedBy: [
      'https://i.imgur.com/zpgE5G3.jpg',
      'https://i.imgur.com/41siYQN.jpg',
      'https://i.imgur.com/QCeRNNF.jpg'
    ]
  },
  {
    user: {
      name: 'Sean DiCaprio',
      pic: 'https://i.imgur.com/QCeRNNF.jpg'
    },
    address: 'Taukhel Sadak, Chitlang 44100',
    time: '6 hours ago',
    coordinate: {
      latitude: 27.649582,
      longitude: 85.15427
    },
    image: {
      url: 'https://i.imgur.com/uiYlbo5.jpg'
    },
    description:
      'Banjo tote bag bicycle rights, High Life sartorial cray craft beer whatever street art fap. Hashtag typewriter banh mi, squid keffiyeh High.',
    verifiedBy: [
      'https://i.imgur.com/zpgE5G3.jpg',
      'https://i.imgur.com/g72dBO9.jpg',
      'https://i.imgur.com/qUCfvm3.jpg',
      'https://i.imgur.com/raKYbaO.jpg'
    ]
  },
  {
    user: {
      name: 'Arnold Stallone',
      pic: 'https://i.imgur.com/DpVlwUr.jpg'
    },
    address: 'Lagankhel, Lalitpur 44700',
    time: '8 hours ago',
    coordinate: {
      latitude: 27.660813,
      longitude: 85.321329
    },
    image: {
      url: 'https://i.imgur.com/ytAXEK1.jpg'
    },
    description:
      'Banjo tote bag bicycle rights, High Life sartorial cray craft beer whatever street art fap. Hashtag typewriter banh mi, squid keffiyeh High.',
    verifiedBy: [
      'https://i.imgur.com/zpgE5G3.jpg',
      'https://i.imgur.com/z6KKuvF.jpg',
      'https://i.imgur.com/QCeRNNF.jpg'
    ]
  },
  {
    user: {
      name: 'Natalie Johansson',
      pic: 'https://i.imgur.com/qUCfvm3.jpg'
    },
    address: 'New Baneshwor, Kathmandu 44600',
    time: '1 day ago',
    coordinate: {
      latitude: 27.68993,
      longitude: 85.329501
    },
    image: {
      url: 'https://i.imgur.com/pEGDePH.jpg'
    },
    description:
      'Banjo tote bag bicycle rights, High Life sartorial cray craft beer whatever street art fap. Hashtag typewriter banh mi, squid keffiyeh High.',
    verifiedBy: [
      'https://i.imgur.com/zpgE5G3.jpg',
      'https://i.imgur.com/41siYQN.jpg',
      'https://i.imgur.com/z6KKuvF.jpg'
    ]
  }
];

class HomeScreen extends Component {
  state = {
    croudsource: croudsourceData
  };

  renderRow = data => {
    if (!data) {
      return null;
    }
    return (
      <TouchableOpacity
        styleName="flexible"
        onPress={() =>
          this.props.navigation.navigate('PostDetail', { post: data })
        }
      >
        <Image styleName="large-wide" source={{ uri: data.image.url }} />
        <View style={{ padding: 20 }}>
          <Subtitle>{data.address}</Subtitle>
          <Caption>{data.time}</Caption>
        </View>
        <Divider styleName="line" />
      </TouchableOpacity>
    );
  };

  render() {
    const { croudsource } = this.state;
    return (
      <Screen>
        <ListView data={croudsource} renderRow={this.renderRow} />
        <ActionButton
          buttonColor="tomato"
          renderIcon={() => <Icon name="edit" style={{ color: 'white' }} />}
          offsetX={10}
          offsetY={10}
          onPress={() => this.props.navigation.navigate('CreatePost')}
        ></ActionButton>
      </Screen>
    );
  }
}

export default HomeScreen;
