/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import Geolocation from '@react-native-community/geolocation';
import MapViewComponent from './../components/MapViewComponent';

import data from './../data/data.json';

const LATITUDE_DELTA = 0.01;
const LONGITUDE_DELTA = 0.01;

class MapScreen extends Component {
  state = {
    region: {
      latitude: 37.78825,
      longitude: -122.4324,
      latitudeDelta: LATITUDE_DELTA,
      longitudeDelta: LONGITUDE_DELTA
    },
    data: []
  };

  componentDidMount() {
    this.getCurrentPosition();
    this.setState({ data: data });
  }

  getCurrentPosition = () => {
    Geolocation.getCurrentPosition(info => {
      const region = {
        latitude: info.coords.latitude,
        longitude: info.coords.longitude,
        latitudeDelta: LATITUDE_DELTA,
        longitudeDelta: LONGITUDE_DELTA
      };
      this.setState({
        region
      });
    });
  };

  render() {
    return (
      <MapViewComponent
        followUserLocation={true}
        zoomEnabled={true}
        provider="google"
        style={styles.map}
        showsUserLocation={true}
        initialRegion={this.state.region}
        region={this.state.region}
        markers={this.state.data}
      />
    );
  }
}

const styles = StyleSheet.create({
  map: {
    ...StyleSheet.absoluteFillObject
  }
});

export default MapScreen;
