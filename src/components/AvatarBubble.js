import React, { Component } from 'react';
import { View, Image, Icon } from '@shoutem/ui';

class AvatarBubble extends Component {
  state = {
    avatars: this.props.avatars
  };

  renderAvatar = (uri, index) => {
    if (index < 1) {
      return (
        <Image
          key={uri}
          style={{
            borderRadius: 33,
            borderWidth: 5,
            borderColor: 'white'
          }}
          styleName="small"
          source={{
            uri: uri
          }}
        />
      );
    }

    return (
      <Image
        key={uri}
        style={{
          marginLeft: -30,
          borderRadius: 33,
          borderWidth: 5,
          borderColor: 'white'
        }}
        styleName="small"
        source={{
          uri: uri
        }}
      />
    );
  };

  render() {
    const avatars = this.state.avatars.map((uri, index) =>
      this.renderAvatar(uri, index)
    );
    return (
      <View styleName="horizontal" style={{ alignItems: 'center' }}>
        {avatars}
      </View>
    );
  }
}

export default AvatarBubble;
