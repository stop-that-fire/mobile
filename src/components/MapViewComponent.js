/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import MapView, { Marker } from 'react-native-maps';
import React, { Component } from 'react';
import { StyleSheet } from 'react-native';
import fire from './../assets/fire.png';

class MapViewComponent extends Component {
  render() {
    return (
      <MapView
        followUserLocation={this.props.followUserLocation}
        zoomEnabled={this.props.zoomEnabled}
        provider={this.props.provider}
        style={this.props.style}
        showsUserLocation={this.props.showsUserLocation}
        initialRegion={this.props.initialRegion}
        region={this.props.region}
      >
        {this.props.markers.map(marker => (
          <Marker
            key={`${marker.latitude}-${marker.longitude}`}
            coordinate={{
              latitude: marker.latitude,
              longitude: marker.longitude
            }}
            title={marker.acq_date}
            description={`Confidence: ${marker.confidence}`}
            image={fire}
          />
        ))}
      </MapView>
    );
  }
}

const styles = StyleSheet.create({
  map: {
    ...StyleSheet.absoluteFillObject
  }
});

MapViewComponent.defaultProps = {
  followUserLocation: true,
  zoomEnabled: true,
  provider: 'google',
  style: styles.map,
  showsUserLocation: true,
  markers: []
};
export default MapViewComponent;
