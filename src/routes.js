import React from 'react';
import { Icon, Button, Text } from '@shoutem/ui';

import { createAppContainer } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';

import MapScreen from './screens/MapScreen';
import HomeScreen from './screens/HomeScreen';
import CameraScreen from './screens/CameraScreen';
import CreatePostScreen from './screens/CreatePostScreen';
import PostDetailScreen from './screens/PostDetailScreen';

const FONT_FAMILY = {
  BOLD: 'Rubik-Bold',
  REGULAR: 'Rubik-Regular'
};

const HomeStack = createStackNavigator({
  Home: {
    screen: HomeScreen,
    navigationOptions: ({ navigation }) => ({
      title: navigation.state.routeName,
      headerTitleStyle: { fontFamily: FONT_FAMILY.BOLD }
    })
  },
  CreatePost: {
    screen: CreatePostScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'Create Post',
      headerTitleStyle: { fontFamily: FONT_FAMILY.BOLD },
      headerLeft: (
        <Icon
          style={{ fontSize: 35 }}
          onPress={() => navigation.goBack(null)}
          name="left-arrow"
        />
      ),
      headerRight: (
        <Button
          style={{
            backgroundColor: 'tomato',
            borderRadius: 30,
            marginRight: 10
          }}
          onPress={() => navigation.goBack(null)}
        >
          <Text style={{ color: 'white' }}>Upload</Text>
        </Button>
      )
    })
  },
  Camera: {
    screen: CameraScreen,
    navigationOptions: {
      header: null
    }
  },
  PostDetail: {
    screen: PostDetailScreen,
    navigationOptions: ({ navigation }) => ({
      title: 'Post Detail',
      headerTitleStyle: { fontFamily: FONT_FAMILY.BOLD },
      headerLeft: (
        <Icon
          style={{ fontSize: 35 }}
          onPress={() => navigation.goBack(null)}
          name="left-arrow"
        />
      )
    })
  }
});

const MapStack = createStackNavigator({
  Map: {
    screen: MapScreen,
    navigationOptions: ({ navigation }) => ({
      title: navigation.state.routeName,
      headerTitleStyle: { fontFamily: FONT_FAMILY.BOLD },
      headerLeft: null
    })
  }
});

const Routes = createAppContainer(
  createBottomTabNavigator(
    {
      Home: {
        screen: HomeStack,
        navigationOptions: ({ navigation }) => {
          let tabBarVisible = true;
          if (navigation.state.index > 0) {
            tabBarVisible = false;
          }
          return {
            tabBarVisible
          };
        }
      },
      Map: { screen: MapStack }
    },
    {
      defaultNavigationOptions: ({ navigation }) => ({
        tabBarIcon: ({ tintColor }) => {
          const { routeName } = navigation.state;
          let iconName;
          if (routeName === 'Home') {
            iconName = 'home';
          } else if (routeName === 'Map') {
            iconName = 'maps';
          }

          return <Icon name={iconName} style={{ color: tintColor }} />;
        },
        headerTintColor: 'tomato'
      }),
      tabBarOptions: {
        activeTintColor: 'tomato',
        inactiveTintColor: 'gray',
        labelStyle: { fontFamily: FONT_FAMILY.REGULAR }
      }
    }
  )
);

export default Routes;
